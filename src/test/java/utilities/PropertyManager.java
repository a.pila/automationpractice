/*package utilities;

import java.io.FileInputStream;
import java.util.Properties;

public class PropertyManager {

	private static PropertyManager instance;
	private static final Object lock = new Object();
	private static String propertyFilePath = System.getProperty("user.dir")
			+ "\\src\\test\\resources\\configuration.properties";
	private static String url;
	private static String invalidUsername;
	private static String invalidPassword;
	private static String validUsername;
	private static String validPassword;

	 //Create a Singleton instance. We need only one instance of Property Manager.
    public static PropertyManager getInstance () {
        if (instance == null) {
            synchronized (lock) {
                instance = new PropertyManager();
                instance.loadData();
            }
        }
        return instance;
    }

	//Get all configuration data and assign to related fields.
	private void loadData() {
		//Declare a properties object
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream(propertyFilePath));
			// prop.load(this.getClass().getClassLoader().getResourceAsStream("configuration.properties"));
		} catch (Exception e) {
			System.out.println("Configruation properties file cannot be found.");
		}
		
		//Get properties from configuration.properties
		url = prop.getProperty("url");
		invalidUsername = prop.getProperty("invalidUsername");
		invalidPassword = prop.getProperty("invalidPassword");
		validUsername = prop.getProperty("validUsername");
		validPassword = prop.getProperty("validPassword");
	}

	public String getURL() {
		return url;
	}
	
	public String getInvalidUsername() {
		return invalidUsername;
	}
	
	public String getInvalidPassword() {
		return invalidPassword;
	}
	public String getValidUsername() {
		return validUsername;
	}
	
	public String getValidPassword() {
		return validPassword;
	}
	
}
*/