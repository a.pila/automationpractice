package tests;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import ro.dataProvider.DefaultLogin;

public class AddProductToCart_Tests {

    private By goToTshirtsElement = By.cssSelector("#block_top_menu li:nth-of-type(3) [title='T-shirts']");
    private By hoverProductElement = By.cssSelector(".color-list-container ul");
    private By moreButtonElement = By.cssSelector("[title='View'] span");
    private By colorElement = By.cssSelector("[name='Orange']");
    private By plusQuantityElement = By.cssSelector(".icon-plus");
    private By chooseSizeElement = By.cssSelector("#group_1");
    private By addToCartElement = By.xpath("//p[@id='add_to_cart']//span[.='Add to cart']");
    private By verifyNotificationElement = By.cssSelector("[class='layer_cart_product col-xs-12 col-md-6'] h2");
    private By continueShoppingElement = By.cssSelector("[title='Continue shopping'] span");
    private By viewLargerElement = By.xpath("//span[@id='view_full_size']/span[.='View larger']");
    private By hoverPicturesElement = By.cssSelector(".fancybox-image");
    private By closeLargeElement = By.cssSelector(".fancybox-close");
    private By forwardButtonElement = By.cssSelector(".fancybox-next");
    private By writeReviewElement = By.cssSelector ( ".comments_advices [href]" );
    private By qualityStarsElement = By.cssSelector ( "[title='5']" );
    private By qualityCancelElement = By.cssSelector ( "[title='Cancel Rating']" );
    private By titleReviewElement = By.cssSelector ( "[name='title']" );
    private By commentReviewElement = By.cssSelector ( "textarea" );
    private By sendReviewElement = By.cssSelector ( ".fr span" );
    private By expectedReviewNotificationElement = By.cssSelector ( ".fancybox-inner p:nth-child(2)" );
    private By clickOkElement = By.cssSelector ( "[value] span" );

    public AddProductToCart_Tests() {
    }

    @Test
    public void addProductToCart() {
        DefaultLogin instance = new DefaultLogin();
        instance.login();

        WebDriver driver = instance.getDriver();

        WebElement goToTshirts = driver.findElement(goToTshirtsElement);
        goToTshirts.click();

        WebElement hoverProduct = driver.findElement(hoverProductElement);
        WebElement moreButton = driver.findElement(moreButtonElement);

        Actions builder = new Actions(driver);
        builder.moveToElement(hoverProduct).build().perform();
        builder.moveToElement(moreButton).build().perform();
        moreButton.click();

        System.out.println("Page of the product is opened.");

        WebElement plusQuantity = driver.findElement(plusQuantityElement);
        plusQuantity.click();

        WebElement chooseSize = driver.findElement(chooseSizeElement);

        Select selectChooseSize = new Select(chooseSize);
        selectChooseSize.selectByIndex(2);
        System.out.println("Size L is selected");

        WebElement color = driver.findElement(colorElement);
        color.click();

        WebElement addToCart = driver.findElement(addToCartElement);
        addToCart.click();

        try {
            Thread.sleep ( 3000 );
        } catch (InterruptedException e) {
            e.printStackTrace ();
        } // wait until the new window is loaded

        WebElement continueShopping = driver.findElement(continueShoppingElement);
        continueShopping.click();

        driver.quit();
    }

    @Test
    public void viewProductPhotos(){
        DefaultLogin instance = new DefaultLogin();
        instance.login();
        WebDriver driver = instance.getDriver();

        WebElement goToTshirtsView = driver.findElement(goToTshirtsElement);
        goToTshirtsView.click();

        WebElement hoverProductView = driver.findElement(hoverProductElement);
        WebElement moreButtonView = driver.findElement(moreButtonElement);
        Actions actionProduct = new Actions(driver);
        actionProduct.moveToElement(hoverProductView).build().perform();
        actionProduct.moveToElement(moreButtonView).build().perform();
        moreButtonView.click();

        WebElement viewLarger = driver.findElement(viewLargerElement);
        viewLarger.click();

        WebElement hoverPictures = driver.findElement(hoverPicturesElement);
        WebElement forwardButton = driver.findElement(forwardButtonElement);
        Actions actionView = new Actions(driver);
        actionView.moveToElement(hoverPictures).build().perform();
        actionView.moveToElement(forwardButton).build().perform();
        forwardButton.click ();

        try {
            Thread.sleep ( 2000 );
        } catch (InterruptedException e) {
            e.printStackTrace ();
        } // wait until the new window is loaded
        WebElement closeLarge = driver.findElement(closeLargeElement);
        closeLarge.click ();

        driver.quit();
    }

    @Test
    public void writeReview(){
        DefaultLogin instance = new DefaultLogin();
        instance.login();
        WebDriver driver = instance.getDriver();

        WebElement goToTshirtsView = driver.findElement(goToTshirtsElement);
        goToTshirtsView.click();

        WebElement hoverProductView = driver.findElement(hoverProductElement);
        WebElement moreButtonView = driver.findElement(moreButtonElement);
        Actions actionProduct = new Actions(driver);
        actionProduct.moveToElement(hoverProductView).build().perform();
        actionProduct.moveToElement(moreButtonView).build().perform();
        moreButtonView.click();

        WebElement writeReview = driver.findElement ( writeReviewElement );
        writeReview.click ();
        System.out.println ( "Rated to 5 stars." );

        WebElement qualityStars = driver.findElement ( qualityStarsElement );
        qualityStars.click ();

        WebElement qualityCancel = driver.findElement ( qualityCancelElement );
        qualityCancel.click ();
        System.out.println ( "Cancelled rating." );

        WebElement titleReview = driver.findElement ( titleReviewElement );
        titleReview.sendKeys ("Review title");

        WebElement commentReview = driver.findElement ( commentReviewElement );
        commentReview.sendKeys ("Review title");

        WebElement sendReview = driver.findElement ( sendReviewElement );
        sendReview.click ();
        System.out.println ( "Review is sent." );

//        String expectedReviewNotification = "Your comment has been added and will be available once approved by a moderator";
//        String reviewNotificationInstance = instance.getDriver ().findElement ( expectedReviewNotificationElement ).getText ();
//        Assert.assertTrue ( "Your comment has been added and will be available once approved by a moderator", reviewNotificationInstance.contains ( expectedReviewNotification ));
//        System.out.println("Notification message is present on page.");

        WebElement clickOk = driver.findElement ( clickOkElement );
        clickOk.click ();

        driver.quit ();
    }
}
