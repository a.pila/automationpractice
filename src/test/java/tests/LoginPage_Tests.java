package tests;

import ro.dataProvider.ConfigFileReader;
import ro.dataProvider.DefaultLogin;
import ro.dataProvider.LoginFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import static junit.framework.TestCase.assertTrue;

public class LoginPage_Tests {

    private By usernameBy = By.id("email");
    private By passwordBy = By.cssSelector("input[name=passwd]");
    private By clickBy = By.xpath("//button[@id='SubmitLogin']/span");
    private By logoBy = By.xpath("//img[contains(@class, 'logo img-responsive')]");
    private By logOutBy = By.xpath("//a[@class='logout']");
    private By invalidLoginNotification = By.xpath("//div[@id='center_column']//ol/li[.='Authentication failed.']");
    //change the locator
    private By storeLocationText = By.xpath("//section[@id='block_contact_infos']//ul[@class='toggle-footer']/li[@class='']");

    private ConfigFileReader configFileReader = new ConfigFileReader();

    @Before
    public void beforeTest() {
        System.out.println("Before test is called.");
    }

   /* @After
    public void afterTest() {
        System.out.println("After test is called.");
        loginPage.getscreenshot();
    }*/

    @Test
    public void sampleLoginTest() {
        DefaultLogin instance = new DefaultLogin();
        instance.login();

    }

    @Test
    public void validLoginTest() {
        LoginFactory instance = LoginFactory.getInstance();
        instance.openAndMaximizeWindow(configFileReader);
        instance.setValidUsername(configFileReader.getValidUsername(), usernameBy);
        instance.setValidPassword(configFileReader.getValidPassword(), passwordBy);
        instance.clickSubmit(clickBy);
        System.out.println("User is logged in.");

        instance.checkLogo(logoBy);

        instance.clickLogOut(logOutBy);

        System.out.println("User is logged out.");

    }

    @Test
    public void invalidLoginTest() {
        LoginFactory instance = LoginFactory.getInstance();
        instance.openAndMaximizeWindow(configFileReader);
        instance.setInvalidUsername(configFileReader.getInvalidUsername(), usernameBy);
        instance.setInvalidPassword(configFileReader.getInvalidPassword(), passwordBy);
        instance.clickSubmit(clickBy);
        System.out.println("User cannot login.");

        //assert notification is present on page
        assertTrue("Notification is present on page (Unsuccessful login)",
                instance.isElementPresent(invalidLoginNotification));

        //assert text is present on page
        String expectedMessage = "Selenium Framework, Research Triangle Park, North Carolina, USA";
        String message = instance.getDriver().findElement(storeLocationText).getText();
        Assert.assertTrue("Store info text not equiv", message.contains(expectedMessage));

    }

}
