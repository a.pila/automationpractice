package tests;


import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import ro.dataProvider.DefaultLogin;

public class Filtering_Tests {
    private By goToWomenElement = By.xpath ( "//div[@id='block_top_menu']/ul//a[@title='Women']" );
    private By sliderElement = By.xpath ( "//div[@id='layered_price_slider']/div" );

    public Filtering_Tests() {

    }

    @Test
    public void filtering() {
        DefaultLogin instance = new DefaultLogin ();
        instance.login ();
        WebDriver driver = instance.getDriver ();

        WebElement goToTWomenView = driver.findElement ( goToWomenElement );
        goToTWomenView.click ();

        WebElement slider = driver.findElement(sliderElement);

        Actions moveSlider = new Actions (driver);
        Action action = moveSlider.dragAndDropBy(slider, 0, 0).build();

        action.perform();

        System.out.println ( "Results are filtered based your specified criteria." );

        driver.quit();
    }
}

