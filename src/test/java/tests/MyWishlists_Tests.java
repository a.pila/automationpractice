
package tests;


import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pages.MyWishlists;
import ro.dataProvider.DefaultLogin;

public class MyWishlists_Tests extends MyWishlists {

    private By myWishlistsElement = By.cssSelector("[title='My wishlists'] span");
    private By wishlistNameElement = By.cssSelector(".inputTxt");
    private By saveWishlistElement = By.cssSelector(".submit span");
    private By identifyTableElement = By.tagName("table");
    private By viewNotificationElement = By.cssSelector(".alert-warning");
    private By backToAccountElement = By.cssSelector("#mywishlist li:nth-of-type(1) span");

    public MyWishlists_Tests() {
    }

    @Test
    public void createWishlist() {
        DefaultLogin instance = new DefaultLogin();
        instance.login();
        WebDriver driver = instance.getDriver();

        WebElement myWishLists = driver.findElement(myWishlistsElement);
        myWishLists.click();
        System.out.println("My Wishlists is opened.");

        WebElement wishlistName = driver.findElement(wishlistNameElement);
        wishlistName.sendKeys("testWishlist");
        System.out.println("Name of the  new wishlist is entered in Name field.");

        WebElement saveWishlist = driver.findElement(saveWishlistElement);
        saveWishlist.click();
        System.out.println("Wishlist is saved.");

        driver.quit();
    }

    @Test
    public void deleteWishlist() {
        DefaultLogin instance = new DefaultLogin();
        instance.login();
        WebDriver driver = instance.getDriver();

        WebElement myWishLists = driver.findElement(myWishlistsElement);
        myWishLists.click();
        System.out.println("My Wishlists section is opened.");

        WebElement baseTable = driver.findElement(identifyTableElement);
        System.out.println("Table is identified.");

        selectDeleteRow(baseTable,0);
        System.out.println("Specific wishlist is deleted.");

        driver.switchTo().alert().accept();
        System.out.println("Alert message is accepted.");

        driver.quit();
    }

    @Test
    public void viewWishlist(){
        DefaultLogin instance = new DefaultLogin();
        instance.login();

        WebDriver driver = instance.getDriver();

        WebElement viewMyWishLists = driver.findElement(myWishlistsElement);
        viewMyWishLists.click();
        System.out.println("My Wishlists section is opened.");

        WebElement viewBaseTable = driver.findElement(identifyTableElement);
        System.out.println("Table is identified.");

        selectViewRow(viewBaseTable, 1);
        System.out.println("Specific wishlist is displayed.");

        String expectedNotificationMessage = "No products";
        String viewNotificationMessage = instance.getDriver().findElement(viewNotificationElement).getText();
        Assert.assertTrue("No products", viewNotificationMessage.contains(expectedNotificationMessage));
        System.out.println("Notification <No products> is present.");

        WebElement backToAccount = driver.findElement(backToAccountElement);
        backToAccount.click();
        System.out.println("User is returned to <Your Account>.");

        driver.quit();
    }


}


